function generateArrayOfObjects(howMany) {
    var tmpArrayOfObjects = [];
    for (var count = 1; count <= howMany; count++) {
      var tmpObject = new Object();
      tmpObject.name = faker.name.firstName();
      tmpObject.phone = faker.phone.phoneNumberFormat(1);
      tmpObject.address = faker.address.city();
      tmpObject.strImage = '<img src="' + faker.internet.avatar() + '"  alt="' + tmpObject.name + '" />';
      tmpObject.$tr = null;
      tmpArrayOfObjects.push(tmpObject);
    }
    return tmpArrayOfObjects;
  }


  var randomName = faker.name.firstName();
  console.log(randomName);
  var positionArray = ["CEO", "COO", "CFO", "Secretary", "Director of Manufacturing", "Director of Shipping", "Director of PR", "Director of Logistics", "Director of Qualtiy Control", "Chief of Distribution", "Chief of Marketing"];

  function generateTable (rowAmount) {
      console.log("Function started successfully");
      if (rowAmount > 11)
      {
          rowAmount = 11;
      }
      var tableStr = "<table><thead><th>Position</th><th>First Name</th><th>Last Name</th></thead>";
      for (var row = 0; row < rowAmount; row++)
      {
          tableStr += "<tr>";
          tableStr += "<td>" + positionArray[row] + "</td>";
          tableStr += "<td>" + faker.name.firstName() + "</td>";
          tableStr += "<td>" + faker.name.lastName() + "</td>";
          tableStr += "</tr>";
      }
      console.log("Function run successfully");
      tableStr += "</table>";
      return tableStr;
  }

$("#generateButton").on('click', function () {
    console.log("Button pressed successfully");
    var newTable = document.createElement("TABLE");
    newTable.innerHTML = "";
    newTable.innerHTML = generateTable(11);
    $('#mytable').html(newTable);
})
  
$(".topper").on('mouseover', function () {
    $('#totop').css("backgroundColor", "green");
})
$(".topper").on('mouseout', function () {
    $('#totop').css("backgroundColor", "rgba(15, 15, 15, 0.2)");
})

$(".topper").on('click', function () {
    $("html, body").animate({ scrollTop: 0 }, "slow");
})