function isNumeric(value) {
    return !isNaN(parseFloat(value)) && isFinite(value);
}

$('h1').on('click', function () {
    mainErrorArea.text('Solid');
})


// Main Form
var mainErrorArea = $('#mainForm');;
mainErrorArea.innerHTML = "";
mainErrorArea.css('color', 'red');
mainErrorArea.css('fontSize', '16px');
mainErrorArea.css('fontFamily', 'Helvetica');
var fname = $('#fname');
var lname = $('#lname');
var email = $('#email');
var tel = $('#tel');

$('#submit').on('click', function(){
    //Email
    console.log(email.val());
    var re = /\S+@\S+\.\S+/;
    var result = re.test(email.val());
    if (result == true)
    {
        console.log("email: TRUE");
        mainErrorArea.text("");
    } else {
        console.log("email: FALSE");
        mainErrorArea.text("Email must contain proper format: xxxxx@xxxx.xxx");
    }
    // Telephone
    console.log(tel.val());
    var regex = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/
    result = regex.test(tel.val());
    if (result == true)
    {
        console.log("tel: TRUE");
        mainErrorArea = mainErrorArea;
    } else {
        console.log("tel: FALSE");
        mainErrorArea.text("Phone Number must contain proper format: xxx-xxx-xxxx");
    }

})


// Update Form
var updateErrorArea = $('#updateForm');
updateErrorArea.css('color', 'red');
updateErrorArea.css('fontSize', '16px');
updateErrorArea.css('fontFamily', 'Helvetica');
var updateErrorArea2 = $('#updateForm2');
updateErrorArea2.css('color', 'red');
updateErrorArea2.css('fontSize', '16px');
updateErrorArea2.css('fontFamily', 'Helvetica');
var custFname = $('#custfname');
var custLname = $('#custlname');
var custTel = $('#custtel');
var custEmail = $('#custemail');

$('#submitit').on('click', function () {
    var regexer = /[a-zA-Z]{2,}/;
    var resulter = regexer.test(custFname.val());
    console.log(resulter);
    if (resulter == true)
    {
        updateErrorArea.text("");
    } else {
        updateErrorArea.text("First name must have at least 2 characters");
    }
    resulter = regexer.test(custLname.val());
    if (resulter == true)
    {
        updateErrorArea = updateErrorArea;
    } else {
        updateErrorArea.text("Last name must have at least 2 characters");
    }
})

// Change following
$('#submitit').on('click', function () {
    regexer = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/
    resulter = regexer.test(custTel.val());
    console.log(resulter);
    if(resulter == true)
    {
        updateErrorArea2.text("");
    } else {
        updateErrorArea2.text("Phone number must follow the format: xxx-xxx-xxxx");
    }
    regexer = /\S+@\S+\.\S+/;
    resulter = regexer.test(custEmail.val());
    console.log("custEmail: " + resulter);
    if(resulter == true)
    {
        updateErrorArea2 = updateErrorArea;
    } else {
        updateErrorArea2.text("Email must follow the format: xxxxx@xxxx.xxx");
    }
})

// Delete Form
var delTel = $('#delTel');
var deleteErrorArea = $('#deleteForm');
deleteErrorArea.css('fontSize', '16px');
deleteErrorArea.css('fontFamily', 'Helvetica');
deleteErrorArea.css("color", "red");
$('#delete').on('click', function () {
    console.log('succ');
    regexer = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/;
    resultee = regexer.test(delTel.val());
    if (resultee == true)
    {
        deleteErrorArea.text("");
    } else {
        deleteErrorArea.text("Phone number must follow the format: xxx-xxx-xxxx");
    }
})

$(".topper").on('mouseover', function () {
    $('#totop').css("backgroundColor", "green");
})
$(".topper").on('mouseout', function () {
    $('#totop').css("backgroundColor", "rgba(15, 15, 15, 0.2)");
})

$(".topper").on('click', function () {
    $("html, body").animate({ scrollTop: 0 }, "slow");
})
