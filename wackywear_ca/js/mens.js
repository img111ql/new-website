var images = $("img");
console.log("length: " + images.length);
var imagesLength = (images.length - 2);
for(var index = 0; index < imagesLength; index++)
{
    images[index].addEventListener('mouseover', function(){
        $(this).css("border", "4px solid green");
    })
    images[index].addEventListener('mouseleave', function(){
        $(this).css("border", "4px solid maroon");
    })
}

$(".topper").on('mouseover', function () {
    $('#totop').css("backgroundColor", "green");
})
$(".topper").on('mouseout', function () {
    $('#totop').css("backgroundColor", "rgba(15, 15, 15, 0.2)");
})

$(".topper").on('click', function () {
    $("html, body").animate({ scrollTop: 0 }, "slow");
})
