var scrWidth = screen.width;
console.log(scrWidth);
var scrHeight = screen.height;
$('#modal').css("width", scrWidth);
$('#modal').css("height", scrHeight);

$('#topleft').on('mouseover', function () {
    $('#topleft').css('border', '4px solid green');
    $('#topleft').on('mouseleave', function () {
        $('#topleft').css('border', '4px solid maroon');
    })
})

$('#topright').on('mouseover', function () {
    $('#topright').css('border', '4px solid green');
    $('#topright').on('mouseleave', function () {
        $('#topright').css('border', '4px solid maroon');
    })
})

$('#bottomleft').on('mouseover', function () {
    $('#bottomleft').css('border', '4px solid green');

    // var bottomLeftWidth = $('#bottomleft').attr('width')
    // bottomLeftWidth = parseInt(bottomLeftWidth);
    // $('#bottomleft').css('width', bottomLeftWidth * 1.1);
    // var bottomLeftHeight = $('#bottomleft').attr('height')
    // bottomLeftHeight = parseInt(bottomLeftHeight);
    // $('#bottomleft').css('height', bottomLeftHeight * 1.1);

    $('#bottomleft').on('mouseleave', function () {
        $('#bottomleft').css('border', '4px solid maroon');
        // $('#bottomleft').css('width', bottomLeftWidth);
        // $('#bottomleft').css('height', bottomLeftHeight);
    })
})

$('#bottomright').on('mouseover', function () {
    $('#bottomright').css('border', '4px solid green');
    $('#bottomright').on('mouseleave', function () {
        $('#bottomright').css('border', '4px solid maroon');
    })
})
//////////////////////////////
var sideform = $('.signupform');
var extended = false;
sideform.on('mouseover', function () {
    if (extended == false) 
    {
        $('#sideform').animate({right: "-15px"});
        extended = true;
    } else {
        extended = true;
    }
})
$('#stopper').on('mouseover', function () {
    if (extended == true)
    {
        $('#sideform').animate({right: "-160px"});
        extended = false;
    } else {
        extended = false;
    }
})
$('#topright').on('mouseover', function () {
    if (extended == true)
    {
        $('#sideform').animate({right: "-160px"});
        extended = false;
    } else {
        extended = false;
    }
})
$('.topper').on('mouseover', function () {
    if (extended == true)
    {
        $('#sideform').animate({right: "-160px"});
        extended = false;
    } else {
        extended = false;
    }
})
//////////////////////////////
var fname = $('#fname');
var lname = $('#lname');
var email = $('#email');
var tel = $('#tel');
var errorAreaEmail = $('#errorareaemail');
errorAreaEmail.css('color', 'red');
errorAreaEmail.css('fontSize', '16px');
errorAreaEmail.css('fontFamily', 'Helvetica');
var errorAreaFname = $('#errorareafname');
errorAreaFname.css('color', 'red');
errorAreaFname.css('fontSize', '16px');
errorAreaFname.css('fontFamily', 'Helvetica');
var errorAreaLname = $('#errorarealname');
errorAreaLname.css('color', 'red');
errorAreaLname.css('fontSize', '16px');
errorAreaLname.css('fontFamily', 'Helvetica');
var errorAreaTel = $('#errorareatel');
errorAreaTel.css('color', 'red');
errorAreaTel.css('fontSize', '16px');
errorAreaTel.css('fontFamily', 'Helvetica');

$('#submitbutton').on('click', function () {
    regexEmail = /\S+@\S+\.\S+/;
    regexName = /[a-zA-Z]{2,}/;
    regexTel = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/;
    resultEmail = regexEmail.test(email.val());
    resultFname = regexName.test(fname.val());
    resultLname = regexName.test(lname.val());
    resultTel = regexTel.test(tel.val());
    console.log("Email: " + resultEmail);
    console.log("First Name: " + resultFname);
    console.log("Last Name: " + resultLname);
    console.log("Telephone: " + resultTel);
    if(resultEmail == true)
    {
        errorAreaEmail.text("");
    } else {
        errorAreaEmail.text("Email must conform to the format: xxxxx@xxxx.xxx");
    }
    if(resultFname == true)
    {
        errorAreaFname.text("");
    } else {
        errorAreaFname.text("First name must contain at least 2 letters");
        console.log("SSS");
    }
    if(resultLname == true)
    {
        errorAreaLname.text("");
    } else {
        errorAreaLname.text("Last name must contain at least 2 letters");
    }
    if(resultTel == true)
    {
        errorAreaTel.text("");
    } else {
        errorAreaTel.text("Telephone must conform to the format: xxx-xxx-xxxx");
    }
})
//////////////////////////////
$(".topper").on('mouseover', function () {
    $('#totop').css("backgroundColor", "green");
})
$(".topper").on('mouseout', function () {
    $('#totop').css("backgroundColor", "rgba(15, 15, 15, 0.2)");
})

$(".topper").on('click', function () {
    $("html, body").animate({ scrollTop: 0 }, "slow");
})
//////////////////////////////


